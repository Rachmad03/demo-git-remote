<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tugas Git</title>
</head>
<body>
    <h1>Tugas 1</h1>
    <?php
        function tentukan_nilai($number)
        {
            //  kode disini
            if($number >=90 && $number <=100){
                $tentukan = "Sangat Baik<br>";
            }else if($number >=70 && $number <=89){
                $tentukan = "Baik<br>";
            }else if($number >=60 && $number <=79){
                $tentukan = "Cukup<br>";
            }else{
                $tentukan = "Kurang<br>";
            }
            
            return $tentukan;
        }

        //TEST CASES
        echo "Grade Nilai = ". tentukan_nilai(98); //Sangat Baik
        echo "Grade Nilai = ". tentukan_nilai(76); //Baik
        echo "Grade Nilai = ". tentukan_nilai(67); //Cukup
        echo "Grade Nilai = ". tentukan_nilai(43); //Kurang
    ?>
    
</body>
</html>